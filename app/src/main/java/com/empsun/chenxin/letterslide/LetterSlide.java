package com.empsun.chenxin.letterslide;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by chenxin on 2017/9/14.
 */

public class LetterSlide extends View {
    private int mHightLightColor= Color.RED;
    private int mNormalColor=Color.GRAY;
    private int mLetterSize=16;
    private Paint mHightLightPaint;
    private Paint mNormalPaint;
    private String[] letters={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"};
    private String mCurrentLetter;
    private float letterHeight;


    public LetterSlide(Context context) {
        this(context,null);
    }

    public LetterSlide(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public LetterSlide(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.LetterSlide);
        mHightLightColor=array.getColor(R.styleable.LetterSlide_highLightColor,mHightLightColor);
        mNormalColor=array.getColor(R.styleable.LetterSlide_normalColor,mNormalColor);
        mLetterSize= (int) array.getDimension(R.styleable.LetterSlide_letterSize,mLetterSize);

        initPaint(context);


    }

    private void initPaint(Context context) {
        mHightLightPaint=new Paint();
        mHightLightPaint.setAntiAlias(true);
        mHightLightPaint.setTextSize(Densityuitl.dip2px(context,mLetterSize));
        mHightLightPaint.setColor(mHightLightColor);

        mNormalPaint=new Paint();
        mNormalPaint.setAntiAlias(true);
        mNormalPaint.setTextSize(Densityuitl.dip2px(context,mLetterSize));
        mNormalPaint.setColor(mNormalColor);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        /*计算宽度*/
        int textWidth = (int) mHightLightPaint.measureText("A");
        int width=getPaddingLeft()+getPaddingRight()+ textWidth;
        int height=MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*计算 一个字母的高度*/
        letterHeight = ((float)getHeight()/letters.length);

        for (int i = 0; i < letters.length; i++) {
            String letter = letters[i];

            /*字体的宽度*/
            float textWidth = mNormalPaint.measureText(letter);
            /*内容的宽度*/
            float contentWidth = getWidth()-getPaddingRight()-getPaddingLeft();
            /*计算从View坐标系的X轴方向开始画文字*/
            float x=getPaddingLeft()+(contentWidth-textWidth)/2;
            /*计算基线*/
            Paint.FontMetrics fontMetrics = mNormalPaint.getFontMetrics();
            float dy = (fontMetrics.bottom - fontMetrics.top) / 2 - fontMetrics.bottom;
            float baseLine = letterHeight / 2 + (letterHeight * i) + dy;

            if (!letter.equals(mCurrentLetter)){
                canvas.drawText(letter,x,baseLine,mNormalPaint);
            }else{
                canvas.drawText(letter,x,baseLine,mHightLightPaint);
            }


        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        /*计算当前滑动选中的字母是那个*/



        switch (event.getAction()){

            case MotionEvent.ACTION_DOWN:

            case MotionEvent.ACTION_MOVE:
                float y = event.getY();
                int v = (int) (y / letterHeight);

                if (v<0){
                    v=0;
                }else if (v>letters.length-1){
                    v=letters.length-1;
                }


                if (letters[v].equals(mCurrentLetter)){
                    return true;
                }
                mCurrentLetter=letters[v];
                if (listener!=null){
                    listener.onListener(mCurrentLetter,false);
                }
                break;
            case MotionEvent.ACTION_UP:

                if (listener!=null){
                    listener.onListener(mCurrentLetter,true);
                }

                break;


        }



        /*进行重绘*/
        invalidate();

//        return super.onTouchEvent(event);
        return true;
    }

    private OnLetterSlideListener listener;
    public interface OnLetterSlideListener{
        void onListener(String letter,boolean upFlag);
    }
    public void setListener(OnLetterSlideListener listener) {
        this.listener = listener;
    }
}
