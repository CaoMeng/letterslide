package com.empsun.chenxin.letterslide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LetterSlide.OnLetterSlideListener {

    private LetterSlide letter_slide;
    private TextView chooice_letter;
    private LinearLayout chooice_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        letter_slide = (LetterSlide) findViewById(R.id.letter_slide);
        letter_slide.setListener(this);
        chooice_letter = (TextView) findViewById(R.id.chooice_letter);
        chooice_layout = (LinearLayout) findViewById(R.id.chooice_layout);
    }

    @Override
    public void onListener(String letter, boolean upFlag) {

        if (!upFlag){
            chooice_layout.setVisibility(View.VISIBLE);
            chooice_letter.setText(letter);
        }else{
            chooice_layout.setVisibility(View.GONE);
        }

    }
}
